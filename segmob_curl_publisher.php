<?php

/**
 * Class SGMBCURL
 * An example demonstrates how to add a Lead or check Lead status
 * using Segmob API for publishers
 */

class SGMBCURL {

    private $sg_api_key; //a valid Publisher API key
    private $base_url; //the base URL of publishers API

    public function __construct( $sg_api_key, $base_url ) {

        $this->sg_api_key = $sg_api_key;
        $this->base_url = $base_url;
    }

    public function post_lead( $lead_data ) {

        /**
         * $lead_data is an array that contains all needed fields for adding a new lead.
         * For example:
         * $lead_data = array();
         * $lead_data['first_name'] = 'Winnie';
         * $lead_data['last_name'] = 'The Pooh';
         * $lead_data['country'] = 'UK';
         * $lead_data['real_phone_number'] = '447555222222';
         * $lead_data['email'] = 'winnie_the_pooh@gmail.com';
         * */

        $json_lead_data = json_encode($lead_data);

        $ch = curl_init();

        curl_setopt($ch,CURLOPT_URL, $this->base_url . 'lead');
        curl_setopt($ch,CURLOPT_CUSTOMREQUEST,'POST');
        curl_setopt($ch,CURLOPT_HTTPHEADER, array('SG-API-KEY: ' .$this->sg_api_key, 'Content-type: application/json'));
        curl_setopt($ch,CURLOPT_POSTFIELDS, $json_lead_data);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);

        $response = curl_exec($ch);

        $http_code = (string)curl_getinfo( $ch,CURLINFO_HTTP_CODE );

        /**
         * Returns an array with HTTP response code and response body as JSON formatted string
         */

        return array( $http_code, $response );
    }

    public function get_lead_status( $lead_id ) {

        /**
         * $lead_id is an existing Lead ID which status to be checked
         */

        $ch = curl_init();

        curl_setopt($ch,CURLOPT_URL,$this->base_url .'lead/' .$lead_id);
        curl_setopt($ch,CURLOPT_HTTPHEADER, array('SG-API-KEY: ' .$this->sg_api_key, 'Content-type: application/json'));
        curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);

        $response = curl_exec($ch);

        $http_code = (string)curl_getinfo( $ch,CURLINFO_HTTP_CODE );

        /**
         * Returns an array with HTTP response code and response body as JSON formatted string
         */

        return array( $http_code, $response );
    }
}