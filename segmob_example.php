<?php
require __DIR__ .'/segmob_curl_publisher.php';

$base_url = 'https://p.segmob.com/api/v1/'; //The base URL of API for publishers
$sg_api_key = 'asfdkjashfkjasdhf112789'; //Your API key

$publisher = new SGMBCURL( $sg_api_key, $base_url );

/**
 * In this example I create an array with single data lead
 * You can get the data of your leads from database or form Excel file or another data source
 * parse they into an array,
 * create a loop in which on each iteration to call the method "post_lead"
 * and send to it the data of new lead
 */
$lead_data = array();
$lead_data['first_name'] = 'Winnie1';
$lead_data['last_name'] = 'The Pooh1';
$lead_data['country'] = 'UA';
$lead_data['real_phone_number'] = '380677777777';
$lead_data['email'] = 'winnie_the_pooh1@gmail.com';

$result = $publisher->post_lead( $lead_data ); // returns an array with HTTP response code and response body as JSON formatted string

$http_code = $result[0];
$json = $result[1];

try {
    $response = json_decode((string)$json);
} catch (\Exception $e) {
    $response = new ArrayObject();
    $response->message = "Unknown server response";
}

echo ( 'HTTP code - ' .$http_code .'<br>' );
echo ( 'Message - ' .$response->message .'<br>' );

/**
 * Only in the case when you have got success HTTP code (200) you get an ID of added lead also
 * In all other cases you get only a message with reason of error
*/
if ( $http_code == '200' ) {
    echo ( 'Lead ID - ' .$response->lead_id .'<br>' );
}



/**
 * In this example I send an ID of single lead
 * You can get the IDs of your leads from database or another data storage
 * parse they into an array,
 * create a loop in which on each iteration to call the method "get_lead_status"
 * and send to it the a lead ID to check it's status
 */
$lead_id = '9409';

$result = $publisher->get_lead_status( $lead_id );

$http_code = $result[0];
$json = $result[1];

try {
    $response = json_decode((string)$json);
} catch (\Exception $e) {
    $response = new ArrayObject();
    $response->message = "Unknown server response";
}

echo ( 'HTTP code - ' .$http_code .'<br>' );
echo ( 'Message - ' .$response->message .'<br>' );

/**
 * Only in the case when you have got success HTTP code (200) you get a status of a lead
 * In all other cases you get only a message with reason of error
 */
if ( $http_code == '200' ) {
    echo ( 'Lead status - ' .$response->status .'<br>' );
}